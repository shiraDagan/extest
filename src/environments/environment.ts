// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBNycFyKXMvsZ2X2cJ5aNR6eI_hdVO1MoY",
    authDomain: "extest-4bdde.firebaseapp.com",
    databaseURL: "https://extest-4bdde.firebaseio.com",
    projectId: "extest-4bdde",
    storageBucket: "extest-4bdde.appspot.com",
    messagingSenderId: "1037185815049",
    appId: "1:1037185815049:web:2f1d4bbbead822129f9ed8",
    measurementId: "G-LG4CHMDNZZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
