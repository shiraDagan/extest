import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  title: string = 'exampleTest';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    constructor(private breakpointObserver: BreakpointObserver, 
      public authService:AuthService,
      location: Location, 
      router: Router){

    router.events.subscribe(val => {
      // if (location.path() == "/books" || location.path() == "/bookform") {
      //   this.title = 'Books';      
       if (location.path() == "/success_login"){
        this.title = "Welcome";
      } else if (location.path() == "/classified_articles"){
        this.title = "Classified articles";
      } else {
        this.title = "Classify form";
      }
    });   
  }

}
