import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-list-classify',
  templateUrl: './list-classify.component.html',
  styleUrls: ['./list-classify.component.css']
})
export class ListClassifyComponent implements OnInit {
  //ex6
  articals$;

  constructor(private service:ClassifyService,public auth:AuthService) { }

  ngOnInit() {
 
  //ex6
  //כאן נקרא לפונקציה שבסרוויס
  this.auth.user.subscribe(
    user => {
      this.articals$ = this.service.getarticles( user.uid);
     }
   )
  }

}
