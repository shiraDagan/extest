import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  url:string;
  text:string;
  

  constructor(private classify:ClassifyService,private router:Router, public imageService:ImageService) { }

 

  onSubmit(){
    this.classify.doc = this.text;
    this.router.navigate(['/classified_articles']);
  }
  ngOnInit() {
  }

}
