import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { ImageService } from '../image.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-classified-articles',
  templateUrl: './classified-articles.component.html',
  styleUrls: ['./classified-articles.component.css']
})
export class ClassifiedArticlesComponent implements OnInit {

title:string;
// Category:string;
userId:string;
id:string;
Category:string;
articles:any;
articles$:Observable<any>;
 
  category:string = "Loading...";
  categoryImage:string; 
  categories:object[] = 
  [{id:0,cat: 'business'}, {id:1,cat: 'entertainment'}, {id:2,cat: 'politics'},{id:3,cat: 'sport'}, {id:4,cat: 'tech'}];

  //ex5 כאן נעדכן את התמונה והקטגוריה לאחר ששיננו אותה ידנית
  onSelect(value){
    this.categoryImage = this.imageService.images[parseInt(value)];
    this.category = this.classifyService.categories[parseInt(value)];
}
  constructor(public classifyService:ClassifyService,
    public imageService:ImageService, private router: Router,private route:ActivatedRoute,
    private authService:AuthService) { }

 
  ngOnInit() {
//ex6
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
     })

  //ex5 - קבלת תוצאת הסיווג והתמונה הרלוונטית   
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )
  
  }


  //add to firestore
  // onSubmit() {
  //   this.classifyService.addClassifyToFirestore(this.userId,this.title,this.Category);
  //  this.router.navigate(['/list_classify']);
  //   }

  //ex6
  //כאן נגדיר את המשתנים שנרצה להכניס לפונקציה שבסרוויס ונקרא לפונקציה
  addClassifyToFirestore(){
   this.classifyService.addToFirestore(this.category, this.userId, this.classifyService.doc); 
        }
      
  

}
