import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Subject, Observable } from 'rxjs';
import { Users } from './interfaces/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<Users | null>;
  private logInErrorSubject = new Subject<string>();

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }

  //הפונקציה הזאת תעזור לנו לשלוף את המשתמש איפה שנקרא לה
  getUser(){
    return this.user 
  }


  constructor(public afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;//לאחר הכניסה למערכת המששתמש יישמר לנו לתוך משתנה
   }

   signup(email:string, passwoerd:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,passwoerd)
        .then(res => console.log('Successful Signup',res))  
        .catch (
          error => window.alert(error)
           )
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          res => console.log('Succesful Login',res)
        ).catch (
          error => window.alert(error)
           )
          
  }

  Logout(){
    this.afAuth.auth.signOut();  
  }
}