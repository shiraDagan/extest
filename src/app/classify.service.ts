import { Article } from './interfaces/article';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private itemsCollection: AngularFirestoreCollection<Article>;

  private url =  "https://thpok8aqi0.execute-api.us-east-1.amazonaws.com/beta";

  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}

  public doc:string;//יחזיק את הטקסט שנכניס
  //ex6
  
  docCollection:AngularFirestoreCollection;

  constructor(private http:HttpClient, private db: AngularFirestore) { 
    this.itemsCollection = db.collection<Article>('classify');//ex6
  }
  

  //בניית פונקציית הסיווג ex5
  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }

  //ex6
  addToFirestore(_title:string, _userId:string, _body:string){
    const article = {title:_title, userId: _userId, body:_body};
    this.db.collection('classify').add(article);
  }
  
//ex6
//הגדרת הפונקציה שתשלוף לדף זה את הגוף והסיווג
getarticles(userId):Observable<any[]>{
 const collection = this.db.collection<Article>('classify', classi => classi.where('userId', '==', userId))
const classify$ = collection.valueChanges();
return classify$;
}





}
