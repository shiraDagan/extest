import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import { HttpClientModule } from '@angular/common/http';

import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';

// Firebase
import { AngularFireModule } from '@angular/fire';

// Firebase modules
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';



import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { environment } from 'src/environments/environment';
import { ClassifyComponent } from './classify/classify.component';
import { ClassifiedArticlesComponent } from './classified-articles/classified-articles.component';
import { SuccessLoginComponent } from './success-login/success-login.component';
import { HomeComponent } from './home/home.component';
import { ListClassifyComponent } from './list-classify/list-classify.component';


const appRoutes: Routes = [
 
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'classify', component: ClassifyComponent },
  { path: 'classified_articles', component: ClassifiedArticlesComponent },
  { path: 'success_login', component: SuccessLoginComponent },
  { path: 'list_classify', component: ListClassifyComponent },

 


  { path: '',
  redirectTo: '/classify',
  pathMatch: 'full'
}
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    ClassifyComponent,
    ClassifiedArticlesComponent,
    SuccessLoginComponent,
    HomeComponent,
    ListClassifyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    AngularFireModule,
    MatCardModule,
    MatFormFieldModule,
    AngularFirestoreModule,
    MatSelectModule,
    MatInputModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    AngularFireAuthModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [AngularFireAuth,],
  bootstrap: [AppComponent]
})
export class AppModule { }
