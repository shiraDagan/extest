import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth:AuthService,private router:Router) {
    this.auth.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
   }

  
  email:string;
  password:string; 
  private errorMessage:string;

  onSubmit(){
    this.auth.login(this.email,this.password);
    this.router.navigate(['/success_login']);
  }
  ngOnInit() {
  }

}
